### semantic segmentation 
### based on VGG 19 layered net

import numpy as np
import tensorflow as tf
import os
import scipy
import scipy.io
import utils as utils
from ImageBatchReader import ImageBatchReader
import cv2
import datetime

NUM_OF_CLASSESS = 2

class FCN_VGG19:

	def __init__(self, pretrain_weights=None):
		if pretrain_weights is not None:
			if not os.path.exists(pretrain_weights):
				print("no weights file found, downloading it")
				MODEL_URL = 'http://www.vlfeat.org/matconvnet/models/beta16/imagenet-vgg-verydeep-19.mat'
				os.system('wget '+MODEL_URL)
			vgg_data = scipy.io.loadmat('imagenet-vgg-verydeep-19.mat')
			mean = vgg_data['normalization'][0][0][0]
			self.mean_pixel = np.mean(mean, axis=(0,1))
			self.weights = np.squeeze(vgg_data['layers'])
			print("weights file loaded")
		else:
			print('please provide weights file to initialize')

	def net(self, img, keep_prob):
		### zero centering
		image = img - self.mean_pixel

		### VGG 19 part of the network
		layers = (
				'conv1_1', 'relu1_1', 'conv1_2', 'relu1_2', 'pool1',

				'conv2_1', 'relu2_1', 'conv2_2', 'relu2_2', 'pool2',

				'conv3_1', 'relu3_1', 'conv3_2', 'relu3_2', 'conv3_3',
				'relu3_3', 'conv3_4', 'relu3_4', 'pool3',

				'conv4_1', 'relu4_1', 'conv4_2', 'relu4_2', 'conv4_3',
				'relu4_3', 'conv4_4', 'relu4_4', 'pool4',

				'conv5_1', 'relu5_1', 'conv5_2', 'relu5_2', 'conv5_3',
				'relu5_3', 'conv5_4', 'relu5_4'
				)


		image_net = {}
		current = image

		for i, name in enumerate(layers):
			kind = name[:4]
			if kind == 'conv':
				kernels, bias = self.weights[i][0][0][0][0]
				# matconvnet: weights are [width, height, in_channels, out_channels]
				# tensorflow: weights are [height, width, in_channels, out_channels]
				kernels = utils.get_variable(np.transpose(kernels, (1, 0, 2, 3)), name=name + "_w")
				bias = utils.get_variable(bias.reshape(-1), name=name + "_b")
				current = utils.conv2d_basic(current, kernels, bias)
			elif kind=='relu':
				current = tf.nn.relu(current, name=name)
			elif kind=='pool':
				current = utils.avg_pool_2x2(current)
			image_net[name] = current


		conv_final_layer = image_net["conv5_3"]

		pool5 = utils.max_pool_2x2(conv_final_layer)

		W6 = utils.weight_variable([7, 7, 512, 4096], name="W6")
		b6 = utils.bias_variable([4096], name="b6")
		conv6 = utils.conv2d_basic(pool5, W6, b6)
		relu6 = tf.nn.relu(conv6, name="relu6")
		# if FLAGS.debug:
		#     utils.add_activation_summary(relu6)
		relu_dropout6 = tf.nn.dropout(relu6, keep_prob=keep_prob)

		W7 = utils.weight_variable([1, 1, 4096, 4096], name="W7")
		b7 = utils.bias_variable([4096], name="b7")
		conv7 = utils.conv2d_basic(relu_dropout6, W7, b7)
		relu7 = tf.nn.relu(conv7, name="relu7")
		# if FLAGS.debug:
		#     utils.add_activation_summary(relu7)
		relu_dropout7 = tf.nn.dropout(relu7, keep_prob=keep_prob)

		W8 = utils.weight_variable([1, 1, 4096, NUM_OF_CLASSESS], name="W8")
		b8 = utils.bias_variable([NUM_OF_CLASSESS], name="b8")
		conv8 = utils.conv2d_basic(relu_dropout7, W8, b8)
		# annotation_pred1 = tf.argmax(conv8, dimension=3, name="prediction1")

		# now to upscale to actual image size
		deconv_shape1 = image_net["pool4"].get_shape()
		W_t1 = utils.weight_variable([4, 4, deconv_shape1[3].value, NUM_OF_CLASSESS], name="W_t1")
		b_t1 = utils.bias_variable([deconv_shape1[3].value], name="b_t1")
		conv_t1 = utils.conv2d_transpose_strided(conv8, W_t1, b_t1, output_shape=tf.shape(image_net["pool4"]))
		fuse_1 = tf.add(conv_t1, image_net["pool4"], name="fuse_1")

		deconv_shape2 = image_net["pool3"].get_shape()
		W_t2 = utils.weight_variable([4, 4, deconv_shape2[3].value, deconv_shape1[3].value], name="W_t2")
		b_t2 = utils.bias_variable([deconv_shape2[3].value], name="b_t2")
		conv_t2 = utils.conv2d_transpose_strided(fuse_1, W_t2, b_t2, output_shape=tf.shape(image_net["pool3"]))
		fuse_2 = tf.add(conv_t2, image_net["pool3"], name="fuse_2")

		shape = tf.shape(image)
		deconv_shape3 = tf.stack([shape[0], shape[1], shape[2], NUM_OF_CLASSESS])
		W_t3 = utils.weight_variable([16, 16, NUM_OF_CLASSESS, deconv_shape2[3].value], name="W_t3")
		b_t3 = utils.bias_variable([NUM_OF_CLASSESS], name="b_t3")
		conv_t3 = utils.conv2d_transpose_strided(fuse_2, W_t3, b_t3, output_shape=deconv_shape3, stride=8, name="Final_layer")

		annotation_pred = tf.argmax(conv_t3, dimension=3, name="prediction")


		return tf.expand_dims(annotation_pred, dim=3), conv_t3






INPUT_SIZE=256
MAX_ITER = 2
def train():
	learning_rate = 1e-4
	image_place = tf.placeholder(tf.float32, shape=[None, INPUT_SIZE,INPUT_SIZE,3], name='input_image')
	label_place = tf.placeholder(tf.int32, shape=[None,INPUT_SIZE,INPUT_SIZE,1], name='label')
	keep_probability = tf.placeholder(tf.float32, name="keep_probabilty")

	
	### inference ops
	model= FCN_VGG19(pretrain_weights='imagenet-vgg-verydeep-19.mat')
	pred_annotation, logits = model.net(img=image_place, keep_prob=keep_probability)


	### training ops
	loss = tf.reduce_mean((tf.nn.sparse_softmax_cross_entropy_with_logits(logits=logits,
											labels=tf.squeeze(label_place, squeeze_dims=[3]),
											name="entropy")))
	trainable_var = tf.trainable_variables()

	optimizer = tf.train.AdamOptimizer(learning_rate)

	grads = optimizer.compute_gradients(loss, var_list=trainable_var)

	train_step = optimizer.apply_gradients(grads)
	
	### load imagebatch reader
	data_folder = 'data/kitti_road_data/data_road/'
	data_list = data_folder+'train.txt'
	train_data_reader = ImageBatchReader(data_folder=data_folder, data_list=data_list, train=True)

	data_list = data_folder+'val.txt'
	val_data_reader = ImageBatchReader(data_folder=data_folder, data_list=data_list, train=False)

	## session
	sess = tf.Session()
	sess.run(tf.global_variables_initializer())

	saver = tf.train.Saver()

	for itr in range(MAX_ITER):
		#print("--------->iteration ", itr)
		images, labels = train_data_reader.next_batch(batch_size=1)
		#cv2.imshow('test', labels[0,:,:,0]*255)
		#cv2.waitKey(0)
		feed_dict = {image_place:images, label_place:labels, keep_probability:0.85}

		sess.run(train_step, feed_dict=feed_dict)

		if itr%10 == 0:
			train_loss = sess.run(loss, feed_dict=feed_dict)
			print("step: %d, Train loss:%g" %(itr, train_loss))

		if itr%500 == 0:
			val_images, val_labels = val_data_reader.next_batch(batch_size=1)
			valid_loss = sess.run(loss, feed_dict={image_place: val_images, label_place: val_labels,
                                                       keep_probability: 1.0})
			print("%s ---> Validation_loss: %g" % (datetime.datetime.now(), valid_loss))
			saver.save(sess, "logs/model.cpkt", itr)

		if itr == MAX_ITER-1:
			print('Final iteration, dumping the models into logs folder')
			saver.save(sess, "logs/model.cpkt", itr)




def test():

	logs_dir = 'logs/'

	sess = tf.Session()

	saver = tf.train.import_meta_graph(logs_dir+'model.cpkt-1.meta')
	saver.restore(sess,tf.train.latest_checkpoint(logs_dir))

	graph = tf.get_default_graph()
	image_place = graph.get_tensor_by_name("input_image:0")
	label_place = graph.get_tensor_by_name("label:0")
	keep_probability = graph.get_tensor_by_name("keep_probabilty:0")

	data_folder = 'data/kitti_road_data/data_road/'
	data_list = data_folder+'val.txt'
	val_data_reader = ImageBatchReader(data_folder=data_folder, data_list=data_list, train=False)
	val_images, val_labels = val_data_reader.next_batch(batch_size=1)

	pred_annotation = graph.get_tensor_by_name("prediction:0")

	pred = sess.run(pred_annotation, feed_dict={image_place: val_images, label_place: val_labels,
	                                                keep_probability: 1.0})
	output = pred[0]
	cv2.imshow("output", val_data_reader.class2color(output))
	#print(output)
	cv2.waitKey(0) 







	'''


	image_place = tf.placeholder(tf.float32, shape=[None, INPUT_SIZE,INPUT_SIZE,3], name='input_image')
	label_place = tf.placeholder(tf.int32, shape=[None,INPUT_SIZE,INPUT_SIZE,1], name='label')
	keep_probability = tf.placeholder(tf.float32, name="keep_probabilty")


	#model= FCN_VGG19(pretrain_weights='imagenet-vgg-verydeep-19.mat')
	pred_annotation, logits = model.net(img=image_place, keep_prob=1.0)

	data_list = data_folder+'val.txt'
	val_data_reader = ImageBatchReader(data_folder=data_folder, data_list=data_list, train=False)
	val_images, val_labels = val_data_reader.next_batch(batch_size=1)

	sess = tf.Session()

	print("Setting up Saver...")
	saver = tf.train.Saver()
	
	ckpt = tf.train.get_checkpoint_state(logs_dir)
	if ckpt and ckpt.model_checkpoint_path:
	    saver.restore(sess, ckpt.model_checkpoint_path)
	    print("Model restored...")

	pred = sess.run(pred_annotation, feed_dict={image_place: val_images, label_place: val_labels,
	                                                keep_probability: 1.0})
	'''


if __name__=='__main__':
	train()
	test()