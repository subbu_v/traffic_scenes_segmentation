import numpy as np
import cv2
import tensorflow as tf
import os
import sys
import scipy as scp
import scipy.misc
import math
import zipfile
import itertools
import random
from random import shuffle


VGG_MEAN = [103.939, 116.779, 123.68]  # imagenet mean bgr

class FCNmodel:

	def __init__(self):
		self.wd = 5e-4

	def weight_variable(self, shape):
		initial = tf.truncated_normal(shape, stddev=0.1)
		return tf.Variable(initial)

	def bias_variable(self, shape):
		initial = tf.constant(0.1, shape=shape)
		return tf.Variable(initial)

	def net(self, bgr, train=False, num_classes=20):
		""" network architecture """

		# with tf.name_scope('zero_centering'):
		# 	r,g,b = tf.split(rgb, 3, 3)
		# 	bgr = tf.concat( [b-VGG_MEAN[0] ,g-VGG_MEAN[1],r-VGG_MEAN[2]], 3)

		### first convolutinal block 
		filters = self.weight_variable(shape=(3,3,3,64))
		biases = self.bias_variable(shape=(64,))
		self.conv1_1 = tf.nn.conv2d(input=bgr, filter=filters, strides=[1,1,1,1], padding='SAME')  #convolution
		self.conv1_1 = tf.nn.bias_add(self.conv1_1, biases) # add bias
		self.conv1_1 = tf.nn.relu(self.conv1_1)#relu

		### second convolutinal block 
		filters = self.weight_variable(shape=(3,3,64,64))
		biases = self.bias_variable(shape=(64,))
		self.conv1_2 = tf.nn.conv2d(input=self.conv1_1, filter=filters, strides=[1,1,1,1], padding='SAME')  #convolution
		self.conv1_2 = tf.nn.bias_add(self.conv1_2, biases) # add bias
		self.conv1_2 = tf.nn.relu(self.conv1_2)#relu


		### first max pooling layer
		self.pool1 = tf.nn.max_pool(self.conv1_2, ksize=[1,2,2,1], strides=[1,2,2,1], padding='SAME', name='pool1')

		### third convolutional block 
		filters = self.weight_variable(shape=(3,3,64,128))
		biases = self.bias_variable(shape=(128,))
		self.conv2_1 = tf.nn.conv2d(input=self.pool1, filter=filters, strides=[1,1,1,1], padding='SAME')  #convolution
		self.conv2_1 = tf.nn.bias_add(self.conv2_1, biases) # add bias
		self.conv2_1 = tf.nn.relu(self.conv2_1)#relu

		### fourth convolutional block 
		filters = self.weight_variable(shape=(3,3,128,128))
		biases = self.bias_variable(shape=(128,))
		self.conv2_2 = tf.nn.conv2d(input=self.conv2_1, filter=filters, strides=[1,1,1,1], padding='SAME')  #convolution
		self.conv2_2 = tf.nn.bias_add(self.conv2_2, biases) # add bias
		self.conv2_2 = tf.nn.relu(self.conv2_2)#relu


		### second max pooling layer
		self.pool2 = tf.nn.max_pool(self.conv2_2, ksize=[1,2,2,1], strides=[1,2,2,1], padding='SAME', name='pool2')

		### fifth convolutional block 
		filters = self.weight_variable(shape=(3,3,128,256))
		biases = self.bias_variable(shape=(256,))
		self.conv3_1 = tf.nn.conv2d(input=self.pool2, filter=filters, strides=[1,1,1,1], padding='SAME')  #convolution
		self.conv3_1 = tf.nn.bias_add(self.conv3_1, biases) # add bias
		self.conv3_1 = tf.nn.relu(self.conv3_1)#relu

		### sixth convolutional block 
		filters = self.weight_variable(shape=(3,3,256,256))
		biases = self.bias_variable(shape=(256,))
		self.conv3_2 = tf.nn.conv2d(input=self.conv3_1, filter=filters, strides=[1,1,1,1], padding='SAME')  #convolution
		self.conv3_2 = tf.nn.bias_add(self.conv3_2, biases) # add bias
		self.conv3_2 = tf.nn.relu(self.conv3_2)#relu

		### seventh convolutional block 
		filters = self.weight_variable(shape=(3,3,256,256))
		biases = self.bias_variable(shape=(256,))
		self.conv3_3 = tf.nn.conv2d(input=self.conv3_2, filter=filters, strides=[1,1,1,1], padding='SAME')  #convolution
		self.conv3_3 = tf.nn.bias_add(self.conv3_3, biases) # add bias
		self.conv3_3 = tf.nn.relu(self.conv3_3)#relu

		## third max pooling layer
		self.pool3 = tf.nn.max_pool(self.conv3_3, ksize=[1,2,2,1], strides=[1,2,2,1], padding='SAME', name='pool3')


		### 8 convolutional block 
		filters = self.weight_variable(shape=(3,3,256,512))
		biases = self.bias_variable(shape=(512,))
		self.conv4_1 = tf.nn.conv2d(input=self.pool3, filter=filters, strides=[1,1,1,1], padding='SAME')  #convolution
		self.conv4_1 = tf.nn.bias_add(self.conv4_1, biases) # add bias
		self.conv4_1 = tf.nn.relu(self.conv4_1)#relu

		### 9 convolutional block 
		filters = self.weight_variable(shape=(3,3,512,512))
		biases = self.bias_variable(shape=(512,))
		self.conv4_2 = tf.nn.conv2d(input=self.conv4_1, filter=filters, strides=[1,1,1,1], padding='SAME')  #convolution
		self.conv4_2 = tf.nn.bias_add(self.conv4_2, biases) # add bias
		self.conv4_2 = tf.nn.relu(self.conv4_2)#relu

		### 10 convolutional block 
		filters = self.weight_variable(shape=(3,3,512,512))
		biases = self.bias_variable(shape=(512,))
		self.conv4_3 = tf.nn.conv2d(input=self.conv4_2, filter=filters, strides=[1,1,1,1], padding='SAME')  #convolution
		self.conv4_3 = tf.nn.bias_add(self.conv4_3, biases) # add bias
		self.conv4_3 = tf.nn.relu(self.conv4_3)#relu

		## 4 max pooling layer
		self.pool4 = tf.nn.max_pool(self.conv4_3, ksize=[1,2,2,1], strides=[1,2,2,1], padding='SAME', name='pool4')


		### 11 convolutional block 
		filters = self.weight_variable(shape=(3,3,512,512))
		biases = self.bias_variable(shape=(512,))
		self.conv5_1 = tf.nn.conv2d(input=self.pool4, filter=filters, strides=[1,1,1,1], padding='SAME')  #convolution
		self.conv5_1 = tf.nn.bias_add(self.conv5_1, biases) # add bias
		self.conv5_1 = tf.nn.relu(self.conv5_1)#relu

		### 12 convolutional block 
		filters = self.weight_variable(shape=(3,3,512,512))
		biases = self.bias_variable(shape=(512,))
		self.conv5_2 = tf.nn.conv2d(input=self.conv5_1, filter=filters, strides=[1,1,1,1], padding='SAME')  #convolution
		self.conv5_2 = tf.nn.bias_add(self.conv5_2, biases) # add bias
		self.conv5_2 = tf.nn.relu(self.conv5_2)#relu

		### 13 convolutional block 
		filters = self.weight_variable(shape=(3,3,512,512))
		biases = self.bias_variable(shape=(512,))
		self.conv5_3 = tf.nn.conv2d(input=self.conv5_2, filter=filters, strides=[1,1,1,1], padding='SAME')  #convolution
		self.conv5_3 = tf.nn.bias_add(self.conv5_3, biases) # add bias
		self.conv5_3 = tf.nn.relu(self.conv5_3)#relu

		## 5 max pooling layer
		self.pool5 = tf.nn.max_pool(self.conv5_3, ksize=[1,2,2,1], strides=[1,2,2,1], padding='SAME', name='pool5')



		## fc layers in VGG16 are replaced by convoluational layers
		# fc6
		filters = self.weight_variable(shape=(7,7,512,4096))
		biases = self.bias_variable(shape=(4096,))
		self.fc6 = tf.nn.conv2d(input=self.pool5, filter=filters, strides=[1,1,1,1], padding='SAME')
		self.fc6 = tf.nn.bias_add(self.fc6, biases)
		self.fc6 = tf.nn.relu(self.fc6)
		if train:
			self.fc6 = tf.nn.dropout(self.fc6, 0.5)

		# fc7
		filters = self.weight_variable(shape=(1,1,4096,4096))
		biases = self.bias_variable(shape=(4096,))
		self.fc7 = tf.nn.conv2d(input=self.fc6, filter=filters, strides=[1,1,1,1], padding='SAME')
		self.fc7 = tf.nn.bias_add(self.fc7, biases)
		self.fc7 = tf.nn.relu(self.fc7)
		if train:
			self.fc7 = tf.nn.dropout(self.fc7, 0.5)


		## score_fr 
		filters = self.weight_variable(shape=(1,1,4096,num_classes))
		biases = self.bias_variable(shape=(num_classes,))
		self.score_fr = tf.nn.conv2d(input=self.fc7, filter=filters, strides=[1,1,1,1], padding='SAME')
		self.score_fr = tf.nn.bias_add(self.score_fr, biases)

		## prediction at fc8
		self.pred_down = tf.argmax(self.score_fr, dimension=3)

		## now upsampling layers
		self.upscore2 = self._upscore_layer(self.score_fr,
		                                    shape=tf.shape(self.pool4),
		                                    num_classes=num_classes,
		                                    name='upscore2',
		                                    ksize=4, stride=2)
		self.score_pool4 = self._score_layer(self.pool4, "score_pool4",
		                                     num_classes=num_classes)
		self.fuse_pool4 = tf.add(self.upscore2, self.score_pool4)

		self.upscore4 = self._upscore_layer(self.fuse_pool4,
		                                    shape=tf.shape(self.pool3),
		                                    num_classes=num_classes,
		                                    name='upscore4',
		                                    ksize=4, stride=2)
		self.score_pool3 = self._score_layer(self.pool3, "score_pool3",
		                                     num_classes=num_classes)
		self.fuse_pool3 = tf.add(self.upscore4, self.score_pool3)

		self.upscore32 = self._upscore_layer(self.fuse_pool3,
		                                     shape=tf.shape(bgr),
		                                     num_classes=num_classes,
		                                     name='upscore32',
		                                     ksize=16, stride=8)

		self.pred_up = tf.argmax(self.upscore32, dimension=3)

	def get_deconv_filter(self, f_shape):
		width = f_shape[0]
		heigh = f_shape[0]
		f = math.ceil(width/2.0)
		c = (2 * f - 1 - f % 2) / (2.0 * f)
		bilinear = np.zeros([f_shape[0], f_shape[1]])
		for x in range(width):
			for y in range(heigh):
				value = (1 - abs(x / f - c)) * (1 - abs(y / f - c))
				bilinear[x, y] = value
		weights = np.zeros(f_shape)
		for i in range(f_shape[2]):
			weights[:, :, i, i] = bilinear

		init = tf.constant_initializer(value=weights, dtype=tf.float32)
		var = tf.get_variable(name="up_filter", initializer=init, shape=weights.shape)
		return var

	def _score_layer(self, inputlayer, name, num_classes):
		with tf.variable_scope(name) as scope:
			## get the number of input channel
			in_features = inputlayer.get_shape()[3].value
			shape = [1,1,in_features, num_classes]
			# initialization schemes
			if name=="score_pool4":
				stddev = 0.001
			elif name=="score_pool3":
				stddev=0.0001

			## convolution
			w_decay = self.wd
			weights = self.weight_variable(shape)   ## TODO add weight decay
			conv = tf.nn.conv2d(inputlayer, weights, [1,1,1,1], padding='SAME')
			## bias
			biases = self.bias_variable([num_classes])
			bias = tf.nn.bias_add(conv, biases)
			return bias

	def _upscore_layer(self, bottom, shape, num_classes, name, ksize=4, stride=2):
		strides = [1, stride, stride, 1]
		with tf.variable_scope(name):
			in_features = bottom.get_shape()[3].value

			if shape is None:
				# Compute shape out of Bottom
				in_shape = tf.shape(bottom)

				h = ((in_shape[1] - 1) * stride) + 1
				w = ((in_shape[2] - 1) * stride) + 1
				new_shape = [in_shape[0], h, w, num_classes]
			else:
				new_shape = [shape[0], shape[1], shape[2], num_classes]
			output_shape = tf.stack(new_shape)

			f_shape = [ksize, ksize, num_classes, in_features]

			# create
			num_input = ksize * ksize * in_features / stride
			stddev = (2 / num_input)**0.5

			weights = self.get_deconv_filter(f_shape)
			deconv = tf.nn.conv2d_transpose(bottom, weights, output_shape,
                                            strides=strides, padding='SAME')
		return deconv

	def loss(self):
		logits = self.pred_up
		logits = tf.reshape(logits, (-1, num_classes))
		epsilon = tf.constant(value=1e-4)
		labels = tf.to_float(tf.reshape(labels, (-1, num_classes)))

		softmax = tf.nn.softmax(logits) + epsilon

		cross_entropy = -tf.reduce_sum(labels * tf.log(softmax), reduction_indices=[1])
		cross_entropy_mean = tf.reduce_mean(cross_entropy,name='xentropy_mean')
		return cross_entropy_mean



DATA_FOLDER = "/home/subbu/Workspace/DATA/kitti_road_data/"
kitti_url = "http://kitti.is.tue.mpg.de/kitti/data_road.zip"
def download_kitti_data():
	data_road_zip = DATA_FOLDER+"data_road.zip"
	if not os.path.exists(data_road_zip):
		print("kitti road dataset is not found,, downloading it -------------------->")
		os.system("wget "+kitti_url+" -P "+DATA_FOLDER)

	## extract the data
	if not os.path.exists(DATA_FOLDER+"data_road/"):
		zipfile.ZipFile(data_road_zip, 'r').extractall(DATA_FOLDER)

	kitti_road_dir = DATA_FOLDER+"data_road/"
	## prepate text files
	trainval_txt = "trainval.txt"
	test_txt = "test.txt"
	val_txt = "val.txt"
	train_txt = "train.txt"

	if not os.path.exists(kitti_road_dir+trainval_txt):
		print("train.txt is not found, so creating ------->")
		with open(kitti_road_dir+trainval_txt, "w") as f:
			training_img_folder = "training/image_2/"
			training_gt_folder = "training/gt_image_2/"
			for item in os.listdir(kitti_road_dir+training_img_folder):
				imgname_splits = item.split('_')
				gt_name = imgname_splits[0]+"_road_"+imgname_splits[1]
				f.write(training_img_folder+item+" "+training_gt_folder+gt_name+"\n")

	if not os.path.exists(kitti_road_dir+test_txt):
		print("test.txt is not found, so creating ------->")
		with open(kitti_road_dir+test_txt, "w") as f:
			testing_img_folder = "testing/image_2/"
			for item in os.listdir(kitti_road_dir+testing_img_folder):
				f.write(testing_img_folder+item+"\n")

	if not os.path.exists(kitti_road_dir+val_txt):
		print("val.txt is not found, so splitting trainval---------->")
		trainval_list =  open(kitti_road_dir+trainval_txt, "r").readlines()
		num_images = len(trainval_list)
		train_list = trainval_list[0:num_images-50]
		val_list = trainval_list[num_images-50:-1]
		with open(kitti_road_dir+train_txt, "w") as f:
			for item in train_list:
				f.write(item)
		with open(kitti_road_dir+val_txt, "w") as f:
			for item in val_list:
				f.write(item)



def read_labeled_image_list(data_dir, data_list):
	"""Reads txt file containing paths to images and ground truth masks.

	Args:
	  data_dir: path to the directory with images and masks.
	  data_list: path to the file with lines of the form '/path/to/image /path/to/mask'.
	   
	Returns:
	  Two lists with all file names for images and masks, respectively.
	"""
	f = open(data_list, 'r')
	images = []
	masks = []
	for line in f:
		image, mask = line.strip("\n").split(' ')
		images.append(data_dir + image)
		masks.append(data_dir + mask)
	return images, masks


IMG_MEAN = np.array((104.00698793,116.66876762,122.67891434), dtype=np.float32)

def read_images_from_disk(input_queue, input_size, random_scale): 
	"""Read one image and its corresponding mask with optional pre-processing.

	Args:
	  input_queue: tf queue with paths to the image and its mask.
	  input_size: a tuple with (height, width) values.
	              If not given, return images of original size.
	  random_scale: whether to randomly scale the images prior
	                to random crop.
	  
	Returns:
	  Two tensors: the decoded image and its mask.
	"""
	img_contents = tf.read_file(input_queue[0])
	label_contents = tf.read_file(input_queue[1])

	img = tf.image.decode_jpeg(img_contents, channels=3)
	label = tf.image.decode_png(label_contents, channels=1)
	if input_size is not None:
		h, w = input_size
		if random_scale:
			scale = tf.random_uniform([1], minval=0.75, maxval=1.25, dtype=tf.float32, seed=None)
			h_new = tf.to_int32(tf.multiply(tf.to_float(tf.shape(img)[0]), scale))
			w_new = tf.to_int32(tf.multiply(tf.to_float(tf.shape(img)[1]), scale))
			new_shape = tf.squeeze(tf.stack([h_new, w_new]), squeeze_dims=[1])
			img = tf.image.resize_images(img, new_shape)
			label = tf.image.resize_nearest_neighbor(tf.expand_dims(label, 0), new_shape)
			label = tf.squeeze(label, squeeze_dims=[0]) # resize_image_with_crop_or_pad accepts 3D-tensor.
		img = tf.image.resize_image_with_crop_or_pad(img, h, w)
		label = tf.image.resize_image_with_crop_or_pad(label, h, w)
	# RGB -> BGR.
	img_r, img_g, img_b = tf.split(axis=2, num_or_size_splits=3, value=img)
	img = tf.cast(tf.concat(axis=2, values=[img_b, img_g, img_r]), dtype=tf.float32)
	# Extract mean.
	img -= IMG_MEAN 
	return img, label

class ImageReader:
	''' Image reader class which reads images and corresponding label maps from the disk and enqueues them into tensorflow queue
	'''
	def __init__(self, data_dir, data_list, input_size, random_scale, coord):
		'''Initializes an Imagereader 
		Args:
			to be written
		'''
		self.data_dir = data_dir
		self.data_list = data_list
		self.input_size = input_size
		self.coord = coord


		self.image_list, self.label_list = read_labeled_image_list(self.data_dir, self.data_list)
		self.images = tf.convert_to_tensor(self.image_list, dtype=tf.string)
		self.labels = tf.convert_to_tensor(self.label_list, dtype=tf.string)
		self.queue = tf.train.slice_input_producer([self.images, self.labels],
		                                           shuffle=input_size is not None) # Not shuffling if it is val.
		self.image, self.label = read_images_from_disk(self.queue, self.input_size, random_scale) 




	def dequeue(self, num_elements):
		'''Pack images and labels into a batch.

		Args:
		  num_elements: the batch size.
		  
		Returns:
		  Two tensors of size (batch_size, h, w, {3,1}) for images and masks.'''
		image_batch, label_batch = tf.train.batch([self.image, self.label],
		                                          num_elements)
		return image_batch, label_batch








def train():
	h,w=384,1248
	input_size=(h,w)
	kitti_data_dir = DATA_FOLDER+"data_road/"
	data_list = kitti_data_dir+"train.txt"
	RANDOM_SCALE = False
	batch_size=1
	lr = 1e-4


	# create the queue coordinator
	coord = tf.train.Coordinator()

	# load the reader
	with tf.name_scope("create_inputs"):
		reader = ImageReader(data_dir=kitti_data_dir, data_list=data_list, input_size=input_size, random_scale=RANDOM_SCALE, coord=coord)
		image_batch, label_batch = reader.dequeue(batch_size)

	'''net = FCNmodel()  ## todoL initilized with pretrained weights

	##### tensors that have to be computed 
	loss =net.loss()
	optimizer = tf.train.AdamOptimizer(learning_rate=lr)
	trainable = tf.trainable_variables()
	gradient_step = optimizer.minimize(loss, var_list=trainable)
	pred = net.preds(image_batch)'''


	sess = tf.Session()
	init = tf.global_variables_initializer()
	sess.run(init)

	for step in range(10):
		print(step)
		images, labels = sess.run([image_batch,label_batch])
		#cv2.imshow('test', images[0])
		#cv2.waitKey(10)

		print(images[0].shape)



train()


























# def load_image_gt(data_dir,data_file=None):
# 	""" takes data file and creates a generator """
# 	files = [line.rstrip() for line in open(data_file)]

# 	for epoch in itertools.count():
# 		shuffle(files)

# 		for file in files:
# 			image_file, gt_file = file.split(" ")
# 			image_file = data_dir+image_file
# 			gt_file = data_dir+gt_file
# 			image = scipy.misc.imread(image_file, mode='RGB')
# 			gt_image = scipy.misc.imread(gt_file, mode='RGB')
# 			yield image, gt_image


# def make_data_sampler(phase, data_dir):
# 	""" samples an image+label from dataset """ 
# 	if phase == 'train':
# 		data_file = data_dir+"train.txt"
# 	elif phase == 'val':
# 		data_file = data_dir+"val.txt"
# 	else:
# 		print("unknown phase")

# 	road_color = np.array([255,0,255]) ## road color in kitti labels
# 	background_color = np.array([255,0,0]) ## background color in kitti labels
	
# 	data = load_image_gt(data_file=data_file, data_dir=data_dir)

# 	for image, gt_image in data:
# 		gt_bg = np.all(gt_image == background_color, axis=2)
# 		gt_road = np.all(gt_image == road_color, axis=2)

# 		assert(gt_road.shape == gt_bg.shape)
# 		shape = gt_bg.shape
# 		gt_bg = gt_bg.reshape(shape[0], shape[1], 1)
# 		gt_road = gt_road.reshape(shape[0], shape[1], 1)

# 		gt_image = np.concatenate((gt_bg, gt_road), axis=2)
# 		yield image, gt_image


# def create_queues(height,width,num_channels,num_classes):
# 	dtypes = [tf.float32, tf.int32]
# 	shapes = [[height,width,num_channels], [height,width, num_classes]]
# 	return tf.FIFOQueue(capacity=50, dtypes=dtypes, shapes=shapes)

# def inputs(q, phase, data_dir):
# 	if phase == 'val':
# 		image, label = q.dequeue()
# 		image = tf.expand_dims(image, 0)
# 		label = tf.expand_dims(label, 0)
# 		return image, label
# 	else:
# 		BATCH_SIZE = 1
# 		image, label = q.dequeue_many(BATCH_SIZE)
# 	return image, label


# def train(max_epoch=1000):
# 	## download the data if it is not there already
# 	download_kitti_data() 

# 	img_height=384
# 	img_width=1248
# 	num_channels=3
# 	num_classes=2

# 	q = create_queues(img_height, img_width, num_channels, num_classes)
# 	kitti_data_dir = DATA_FOLDER+"data_road/"
# 	gen = make_data_sampler(phase='train', data_dir=kitti_data_dir)

# 	image_batch, label_batch = inputs(q, 'train', data_dir=kitti_data_dir)
# 	## do training

# 	init = tf.initialize_all_variables()
# 	sess = tf.Session()
# 	sess.run(init)


# 	image_place = tf.placeholder(tf.float32)
# 	label_place = tf.placeholder(tf.int32)

# 	for epoch in range(max_epoch):
# 		img, label = gen.next()
# 		scipy.misc.imshow(img[0])



	







# def color_image(image, num_classes=20):
#     import matplotlib as mpl
#     import matplotlib.cm
#     norm = mpl.colors.Normalize(vmin=0., vmax=num_classes)
#     mycm = mpl.cm.get_cmap('Set1')
#     return mycm(norm(image))




# train()

# # filename = '/home/subbu/lena.png'
# # img1 = cv2.imread(filename,1)

# # with tf.Session() as sess:
# # 	images = tf.placeholder("float")
# # 	feed_dict = {images:img1}
# # 	batch_images = tf.expand_dims(images, 0)
	
# # 	'''vgg_fcn = fcn8_vgg.FCN8VGG('vgg16.npy')

# # 	with tf.name_scope("content_vgg"):
# # 		vgg_fcn.build(batch_images, debug=True)'''

# # 	myfcn = FCNmodel()
# # 	myfcn.net(batch_images)

# # 	init=tf.global_variables_initializer()
# # 	sess.run(init)

# # 	tensors = [myfcn.pred_up]
# # 	'''
# # 	print("running the network")
# # 	tensors=[vgg_fcn.pred, vgg_fcn.pred_up]

# # 	down, up = sess.run(tensors, feed_dict=feed_dict)

# # 	down_color = color_image(down[0])
# # 	up_color = color_image(up[0])

# # 	scp.misc.imsave('fcn8_downsampled.png', down_color)
# # 	scp.misc.imsave('fcn8_upsampled.png', up_color)
# # 	'''

# # 	out = sess.run(tensors, feed_dict=feed_dict)
# # 	print(out[0][0,:,:].shape)
# # 	cv2.imshow('test',color_image(out[0][0,:,:]))
# # 	cv2.waitKey(0)


# # 	# print(out[0].shape)
# # 	# i = color_image(out[0][0])
# # 	# print(i[:,:,0:3].shape)
# # 	# scp.misc.imsave('test.png', i[:,:,0:3])