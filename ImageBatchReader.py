import numpy as np
import scipy
import scipy.misc as misc
import random

KITTI_DICT = {
	"road_color": [255,0,255],
	"road_id" : 1,
	## data augmentation options
	"random_resize":True,
	"resize_chance":0.4,
	"lower_size" :0.4,
	"higher_size":1.7,
	"sigma"      :0.15,

	"random_crop":True,
	"crop_chance":0.8,
	"patch_height":256,
	"patch_width":256
}



class ImageBatchReader:
	''' a class for image batch reading
	TODO:
		random scaling and random cropping to be added
	'''
	batch_offset = 0
	epochs_completed = 0
	DATA_AUGMENT = True

	def __init__(self, data_folder, data_list, input_size=(256,256), train=True):
		self.data_folder = data_folder
		self.data_list = data_list
		self.input_size = input_size
		self.image_list = []
		self.label_list = []


		with open(data_list, 'r') as f:
			for line in f:
				img, label = line.rstrip('\n').split(' ')
				self.image_list.append(data_folder+img)
				self.label_list.append(data_folder+label)

		self.image_list = np.array(self.image_list)
		self.label_list = np.array(self.label_list)

		if train:
			### shuffle
			perm = np.arange(len(self.image_list))
			np.random.shuffle(perm)
			self.image_list = self.image_list[perm]
			self.label_list = self.label_list[perm]

	def next_batch(self, batch_size=1):
		start = self.batch_offset
		self.batch_offset += batch_size

		if self.batch_offset > len(self.image_list):
			## finished epoch
			self.epochs_completed +=1
			print("****************** Epochs completed: " + str(self.epochs_completed) + "******************")
			# Shuffle the data
			perm = np.arange(len(self.image_list))
			np.random.shuffle(perm)
			self.image_list = self.image_list[perm]
			self.label_list = self.label_list[perm]
			# Start next epoch
			start = 0
			self.batch_offset = batch_size

		end = self.batch_offset
		return self.return_the_batch(start, end)

	def return_the_batch(self, start, end):
		images = []
		labels = []
		for index in range(start, end):
			img_file = self.image_list[index]
			gt_file = self.label_list[index]
			img = misc.imread(img_file, mode='RGB')
			label =misc.imread(gt_file, mode='RGB')

			if self.DATA_AUGMENT:
				img, label = self.jitter(img, label)
				label=self.color2class(label)
			images.append(img)
			labels.append(label)

		return np.array(images), np.expand_dims(np.array(labels), axis=3)

	def read_images(self, start, end):
		images = []
		for file in self.image_list[start:end]:
			img = misc.imread(file, mode='RGB')
			## resize
			resized_img = misc.imresize(img, self.input_size, interp='bilinear')
			images.append(resized_img)
		return np.array(images)

	def read_label_images(self, start, end):
		labels=[]
		for file in self.label_list[start:end]:
			label = misc.imread(file, mode='RGB')
			## resize
			resized_label = misc.imresize(label, self.input_size, interp='nearest')
			## color2class tranform
			resized_label = self.color2class(resized_label)
			labels.append(resized_label)
		return np.expand_dims(np.array(labels), axis=3)

	def color2class(self, img):
		gt = np.zeros((img.shape[0], img.shape[1]), dtype=int)  # only one channel
		road_id = KITTI_DICT["road_id"]
		road_color = KITTI_DICT["road_color"]
		affected_pixels = np.all(img == road_color, axis=2)
		gt += affected_pixels*road_id
		return gt

	def class2color(self, class_img): # class 1 assumed to be foreground
		color_img = np.zeros((class_img.shape[0], class_img.shape[1], 3), dtype=np.uint8)
		road_id = KITTI_DICT["road_id"]
		road_color = KITTI_DICT["road_color"]
		color_img[class_img==road_id]=road_color
		return color_img


	def reset_batch_offset(self):
		self.batch_offset = 0

	### TODO implement some data augmentation methods

	def jitter(self, img, label):

		### random resize
		random_resize = KITTI_DICT["random_resize"]
		resize_chance = KITTI_DICT["resize_chance"]
		if random_resize and resize_chance > random.random():
			lower_size = KITTI_DICT["lower_size"]
			higher_size = KITTI_DICT["higher_size"]
			sigma = KITTI_DICT["sigma"]

			factor = random.normalvariate(1, sigma)
			if factor < lower_size: 
				factor = lower_size
			if factor > higher_size:
				factor = higher_size
			img = misc.imresize(img, factor)
			label = misc.imresize(label, factor, interp='nearest')

		### random crop
		random_crop = KITTI_DICT["random_crop"]
		crop_chance = KITTI_DICT["crop_chance"]
		height      = KITTI_DICT["patch_height"]
		width       = KITTI_DICT["patch_width"]
		if random_crop and crop_chance > random.random():
			old_height = img.shape[0]
			old_width = img.shape[1]
			max_c = max(old_width - width, 0) 
			max_r = max(old_height - height, 0)
			offset_r = random.randint(0, max_r)
			offset_c = random.randint(0, max_c)
			img = img[offset_r:offset_r+height, offset_c:offset_c+width]
			label = label[offset_r:offset_r+height, offset_c:offset_c+width]

		### make sure image size is required input size
		if not img.shape[0] == self.input_size[0]:
			img = misc.imresize(img, self.input_size)
			label = misc.imresize(label, self.input_size, interp='nearest')
		return img, label





def main():
	data_folder = '/home/subbu/Workspace/DATA/kitti_road_data/data_road/'
	data_list = data_folder+'train.txt'

	reader = ImageBatchReader(data_folder=data_folder, data_list=data_list, train=True)
	import cv2
	for i in range(1000):
		image_batch, label_batch = reader.next_batch(batch_size=1)
		label_class_img = label_batch[0,:,:,0]
		label_color_img = reader.class2color(class_img=label_class_img)
		cv2.imshow('test',np.concatenate([image_batch[0], label_color_img], axis=1))
		if cv2.waitKey(0) & 0xFF == ord('q'):
			break

if __name__ == '__main__':
	main()